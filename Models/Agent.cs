﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimInterface.Models
{
    public class Agent
    {
        [Key]
        public int id { get; set; }
        public int id_cm { get; set; }
        public string uuid { get; set; }
        public string label { get; set; }
    }
}
