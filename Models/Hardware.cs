﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimInterface.Models
{
    public class Hardware
    {
        [Key]
        public long id { get; set; }
        public string hwType { get; set; }
        public string status { get; set; }
        public string serialNumber { get; set; }
        public Nullable<DateTime> lastHeartbeat { get; set; }
        public string jsonText { get; set; }
        public Nullable<DateTime> timestamp { get; set; }
        public long id_entry { get; set; }
    }
}
