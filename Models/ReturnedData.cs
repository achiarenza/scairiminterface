﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimInterface.Models
{
    public class ReturnedData
    {
        public string description { get; set; }
        public int requests { get; set; }
        public int processed { get; set; }
    }
}
