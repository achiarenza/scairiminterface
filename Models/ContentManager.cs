﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimInterface.Models
{
    public class ContentManager
    {
        [Key]
        public int id { get; set; }
        public string address { get; set; }
        public string label { get; set; }
        public string platform { get; set; }
        public string timezone { get; set; }
    }
}
