﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ScairimInterface.Functions
{
    public class ScairimLogManager
    {
        public EventLog logger;
        public EventLogEntryType configuredType;

        public ScairimLogManager(string conf, string source)
        {
            if (!EventLog.SourceExists(source)) EventLog.CreateEventSource(source, "Application");

            if (conf.ToLower() == "info" || conf.ToLower() == "information")
            {
                configuredType = EventLogEntryType.Information;
            }
            else if (conf.ToLower() == "warn" || conf.ToLower() == "warning")
            {
                configuredType = EventLogEntryType.Warning;
            }
            else
            {
                configuredType = EventLogEntryType.Error;
            }
            logger = new EventLog();
            logger.Source = source;
        }

        public void Log(string log, EventLogEntryType type)
        {
            if (type == EventLogEntryType.Information && configuredType != EventLogEntryType.Information)
            {
                return;
            }
            else if (type == EventLogEntryType.Warning && configuredType == EventLogEntryType.Error)
            {
                return;
            }
            logger.WriteEntry(log, type);
        }        
    }
}
