﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ScairimInterface.Models
{
    public class LogEntry
    {
        [Key]
        public long id { get; set; }
        public string player_name { get; set; }
        public string uuid { get; set; }
        public string hostname { get; set; }
        public string status { get; set; }
        public string description { get; set; }
        public string inventoryStatus { get; set; }
        public Nullable<DateTime> lastHeartbeat { get; set; }
        public Nullable<DateTime> lastBooted { get; set; }
        public Nullable<DateTime> currentDatetime { get; set; }
        public Nullable<long> clockOffset { get; set; }
        public string screenResolution { get; set; }
        public string timezone { get; set; }
        public Nullable<DateTime> playerTimestamp { get; set; }
        public Nullable<DateTime> lastUpdate { get; set; }
        public string diskSpace { get; set; }
        public string jsonText { get; set; }
        public Nullable<DateTime> timestamp { get; set; }
        public List<string> channel { get; set; }
        public List<string> channelRes { get; set; }
        public List<string> framesetRes { get; set; }
        public string customer { get; set; }
        public long id_customer { get; set; }
        public long id_cm { get; set; }
        public long id_agent { get; set; }
        public List<Hardware> hardware { get; set; }
        public int remainingItems { get; set; }
        public double remainingMB { get; set; }
        public List<string> groups { get; set; }
    }
}
