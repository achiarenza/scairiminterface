﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace ScairimInterface.Models
{
    public class Result
    {
        public string result { get; set; }
        public ReturnedData data { get; set; }
    }
}
