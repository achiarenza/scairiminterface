﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ScairimInterface.Models;
using Newtonsoft.Json.Linq;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using ScairimInterface.Functions;
using System.Text.Json;
using System.Diagnostics;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ScairimInterface.Controllers
{
    //[Route("api/[controller]")]
    [Route("api")]
    [ApiController]
    public class ScairimInterfaceController : ControllerBase
    {
        private ScairimLogManager logger;
        private string[] aAdminSecrets;

        private static string connString;
        public ScairimInterfaceController(IConfiguration configuration)
        {
            connString = configuration["ConnectionStrings:Postgresql"];
            logger = new ScairimLogManager(configuration["Logging:LogLevel:Default"], "ScairimInterface");
            aAdminSecrets = configuration.GetSection("AdmAgent").Get<string[]>();
        }

        #region Dictionaries

        private static object locker_agent = new object();
        private static Dictionary<string, Agent> dic_uuid_agents;
        private static Dictionary<string, Agent> Getdic_uuid_agents
        {
            get 
            {
                if (dic_uuid_agents == null) 
                {
                    lock (locker_agent)
                    {
                        if (dic_uuid_agents == null)
                        {
                            initAgents();
                        }                        
                    }
                }
                return dic_uuid_agents;
            }
        }
        private static object locker_cm = new object();
        private static Dictionary<string, ContentManager> dic_address_CMs;
        private static Dictionary<string, ContentManager> Getdic_address_CMs
        {
            get
            {
                if (dic_address_CMs == null) 
                {
                    lock (locker_cm)
                    {
                        if (dic_address_CMs == null)
                        {
                            initCMs();
                        }                        
                    }
                }
                return dic_address_CMs;
            }
        }
        private static object locker_customer = new object();
        private static Dictionary<string, Customer> dic_name_customer;
        private static Dictionary<string, Customer> Getdic_name_customer
        {
            get
            {
                if (dic_name_customer == null)
                {
                    lock (locker_customer)
                    {
                        if (dic_name_customer == null)
                        {
                            initCustomers();
                        }
                    }
                }
                return dic_name_customer;
            }
        }

        #endregion

        #region GET

        // GET: api/<ScairimInterfaceController>
        [HttpGet]
        public Result Get()
        {
            return GetResult("SUCCESS", "OK", 0, 0);
        }

        // GET api/<ScairimInterfaceController>/CMs
        [HttpGet("CMs")]
        public List<ContentManager> GetCMs()
        {
            List<ContentManager> res = new List<ContentManager>();

            if (!AdminAuthorized(Request.Headers["secret"]))
            {
                logger.Log("Admin Request Not authorized", EventLogEntryType.Warning);
                return res;
            }

            foreach (ContentManager c in Getdic_address_CMs.Values)
            {
                res.Add(c);
            }

            return res;
        }

        // GET api/Customers
        [HttpGet("Customers")]
        public List<Customer> GetClients()
        {
            List<Customer> res = new List<Customer>();

            if (!AdminAuthorized(Request.Headers["secret"]))
            {
                logger.Log("Admin Request Not authorized", EventLogEntryType.Warning);
                return res;
            }

            foreach (Customer c in Getdic_name_customer.Values)
            {
                res.Add(c);
            }

            return res;
        }

        // GET api/<ScairimInterfaceController>/Agents
        [HttpGet("Agents")]
        public List<Agent> GetAgents()
        {
            List<Agent> res = new List<Agent>();

            if (!AdminAuthorized(Request.Headers["secret"]))
            {
                logger.Log("Admin Request Not authorized", EventLogEntryType.Warning);
                return res;
            }

            foreach (Agent a in Getdic_uuid_agents.Values)
            {
                res.Add(a);
            }           

            return res;
        }

        #region UnusedGet
        // GET api/<ScairimInterfaceController>/Hardware
        //[HttpGet("Hardware")]
        //public List<Hardware> GetHardware()
        //{
        //    using var conn = new NpgsqlConnection(connString);
        //    conn.Open();
        //    var sql = "SELECT * FROM hardware";
        //    using var cmd = new NpgsqlCommand(sql, conn);

        //    using var reader = cmd.ExecuteReader();

        //    List<Hardware> res = new List<Hardware>();

        //    if (!Authorized(Request.Headers["uuid"]))
        //    {
        //        return res;
        //    }

        //    while (reader.Read())
        //    {
        //        Hardware h = new Hardware
        //        {
        //            id = reader.GetInt64(reader.GetOrdinal("id")),
        //            hwType = reader.GetString(reader.GetOrdinal("hw_type")),
        //            status = reader.GetString(reader.GetOrdinal("status")),
        //            serialNumber = reader.GetString(reader.GetOrdinal("serialnumber")),
        //            lastHearthbeat = reader.GetDateTime(reader.GetOrdinal("last_hearthbeat")),
        //            jsonText = reader.GetString(reader.GetOrdinal("json_text")),
        //            timestamp = reader.GetDateTime(reader.GetOrdinal("timestamp")),
        //            id_entry = reader.GetInt64(reader.GetOrdinal("id_entry"))
        //        };
        //        res.Add(h);
        //    }

        //    return res;
        //}

        // GET api/<ScairimInterfaceController>/LogEntry
        //[HttpGet("LogEntry")]
        //public List<LogEntry> GetLogEntry()
        //{
        //    using var conn = new NpgsqlConnection(connString);
        //    conn.Open();
        //    var sql = "SELECT * FROM logentry";
        //    using var cmd = new NpgsqlCommand(sql, conn);

        //    using var reader = cmd.ExecuteReader();

        //    List<LogEntry> res = new List<LogEntry>();

        //    if (!Authorized(Request.Headers["uuid"]))
        //    {
        //        return res;
        //    }

        //    while (reader.Read())
        //    {
        //        LogEntry l = new LogEntry
        //        {
        //            id = reader.GetInt64(reader.GetOrdinal("id")),
        //            player_name = reader.GetString(reader.GetOrdinal("player_name")),
        //            status = reader.GetString(reader.GetOrdinal("status")),
        //            description = reader.GetString(reader.GetOrdinal("description")),
        //            uuid = reader.GetString(reader.GetOrdinal("uuid")),
        //            lastHeartbeat = reader.GetDateTime(reader.GetOrdinal("last_hearthbeat")),
        //            jsonText = reader.GetString(reader.GetOrdinal("json_text")),
        //            timestamp = reader.GetDateTime(reader.GetOrdinal("timestamp")),
        //            id_client = reader.GetInt64(reader.GetOrdinal("id_client")),
        //            hostname = reader.GetString(reader.GetOrdinal("hostname")),
        //            lastBooted = reader.GetDateTime(reader.GetOrdinal("last_booted")),
        //            currentDatetime = reader.GetDateTime(reader.GetOrdinal("current_datetime")),
        //            timezone = reader.GetString(reader.GetOrdinal("timezone")),
        //            inventoryStatus = reader.GetString(reader.GetOrdinal("inventory_status")),
        //            clockOffset = reader.GetString(reader.GetOrdinal("clock_offset")),
        //            screenResolution = reader.GetString(reader.GetOrdinal("screen_resolution")),
        //            diskSpace = reader.GetString(reader.GetOrdinal("disk_space")),
        //            channel = StringToStringList(reader.GetString(reader.GetOrdinal("channel"))),
        //            channelRes = StringToStringList(reader.GetString(reader.GetOrdinal("channel_res"))),
        //            framesetRes = StringToStringList(reader.GetString(reader.GetOrdinal("frameset_res"))),
        //            id_agent = reader.GetInt64(reader.GetOrdinal("id_agent")),
        //            id_cm = reader.GetInt64(reader.GetOrdinal("id_cm"))
        //        };
        //        res.Add(l);
        //    }

        //    return res;
        //}
        #endregion

        #endregion

        #region POST

        // POST api/CMs
        [HttpPost("CMs")]
        public Result insertCM([FromBody] List<ContentManager> value)
        {
            try
            {                
                if (!AdminAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Not authorized", EventLogEntryType.Warning);
                    return GetResult("ERROR", "Not Authorized", 0, 0);
                }

                int rows = 0;

                string insert = "INSERT INTO cms (address, label, platform, timezone) VALUES (@address, @label, @platform, @timezone)";

                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            using (var cmd = new NpgsqlCommand(insert, conn, trans))
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@address", NpgsqlTypes.NpgsqlDbType.Text));
                                cmd.Parameters.Add(new NpgsqlParameter("@label", NpgsqlTypes.NpgsqlDbType.Text));
                                cmd.Parameters.Add(new NpgsqlParameter("@platform", NpgsqlTypes.NpgsqlDbType.Text));
                                cmd.Parameters.Add(new NpgsqlParameter("@timezone", NpgsqlTypes.NpgsqlDbType.Text));
                                foreach (ContentManager cm in value)
                                {
                                    if (!Getdic_address_CMs.ContainsKey(cm.address))
                                    {
                                        cmd.Parameters["@address"].Value = cm.address;
                                        cmd.Parameters["@label"].Value = cm.label;
                                        cmd.Parameters["@platform"].Value = cm.platform;
                                        cmd.Parameters["@timezone"].Value = cm.timezone;
                                        rows += cmd.ExecuteNonQuery();
                                    }                                    
                                }
                            }
                            trans.Commit();                            
                        }
                        catch (Exception f)
                        {
                            trans.Rollback();
                            logger.Log(f.Message + " " + f.StackTrace, EventLogEntryType.Error);
                            return GetResult("ERROR", f.Message, value.Count(), 0);
                        }
                    }                    
                }

                lock (locker_cm)
                {
                    dic_address_CMs = null;
                }

                logger.Log("Created " + rows + " rows.", EventLogEntryType.Information);
                return GetResult("SUCCESS", "Created " + rows + " rows.", value.Count(), rows);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                return GetResult("ERROR", e.Message, 0, 0);
            }
        }

        // POST api/Agents
        [HttpPost("Agents")]
        public Result createAgent([FromBody] JsonElement value)
        {
            try
            {
                if (!AdminAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Not authorized", EventLogEntryType.Warning);
                    return GetResult("ERROR", "Not Authorized", 0, 0);
                }

                int rows = 0;
                JObject oJson = new JObject();
                oJson = JObject.Parse(value.ToString());

                string insert = "INSERT INTO agents (id_cm, uuid, label) VALUES (@id_cm, @uuid, @label)";

                using (var conn = new NpgsqlConnection(connString))
                {                    
                    conn.Open();
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            string newUUID = Guid.NewGuid().ToString();
                            using (var cmd = new NpgsqlCommand(insert, conn, trans))
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint));
                                cmd.Parameters.Add(new NpgsqlParameter("@uuid", NpgsqlTypes.NpgsqlDbType.Text));
                                cmd.Parameters.Add(new NpgsqlParameter("@label", NpgsqlTypes.NpgsqlDbType.Text));
                                foreach (JToken t in oJson["list"].Children())
                                {
                                    //cmd.Parameters["@id_cm"].Value = DBNull.Value;
                                    if (t["cm_address"] != null)
                                    {
                                        if (Getdic_address_CMs.ContainsKey((string)t["cm_address"]))
                                        {
                                            cmd.Parameters["@id_cm"].Value = Getdic_address_CMs[(string)t["cm_address"]].id;
                                        }
                                        else
                                        {
                                            throw (new Exception("CM not found in DB"));
                                        }
                                    }
                                    else
                                    {
                                        throw (new Exception("Missing CM"));
                                    }
                                    if (t["uuid"] != null)
                                    {
                                        cmd.Parameters["@uuid"].Value = DBvalue((string)t["uuid"]);
                                    }
                                    else 
                                    {
                                        cmd.Parameters["@uuid"].Value = newUUID;
                                    }                                    
                                    cmd.Parameters["@label"].Value = DBvalue((string)t["label"]);
                                    rows += cmd.ExecuteNonQuery();
                                }
                            }
                            trans.Commit();                            
                        }
                        catch (Exception f)
                        {
                            trans.Rollback();
                            logger.Log(f.Message + " " + f.StackTrace, EventLogEntryType.Error);
                            return GetResult("ERROR", f.Message, oJson["list"].Count<JToken>(), 0);
                        }
                    }
                }

                lock (locker_agent)
                {
                    dic_uuid_agents = null;
                }

                return GetResult("SUCCESS", "Created " + rows + " rows.", oJson["list"].Count<JToken>(), rows);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                return GetResult("ERROR", e.Message, 0, 0);
            }
        }

        // POST api/Customers
        [HttpPost("Customers")]
        public Result insertClient([FromBody] JsonElement value)
        {
            try
            {
                if (!AdminAuthorized(Request.Headers["secret"]))
                {
                    logger.Log("Not authorized", EventLogEntryType.Warning);
                    return GetResult("ERROR", "Not Authorized", 0, 0);
                }

                int rows = 0;
                JObject oJson = new JObject();
                oJson = JObject.Parse(value.ToString());

                string insert = "INSERT INTO customers (id_cm, name, brand) VALUES (@id_cm, @name, @brand)";

                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            using (var cmd = new NpgsqlCommand(insert, conn, trans))
                            {
                                cmd.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint));
                                cmd.Parameters.Add(new NpgsqlParameter("@name", NpgsqlTypes.NpgsqlDbType.Text));
                                cmd.Parameters.Add(new NpgsqlParameter("@brand", NpgsqlTypes.NpgsqlDbType.Text));
                                foreach (JToken t in oJson["list"].Children())
                                {
                                    cmd.Parameters["@id_cm"].Value = DBNull.Value;
                                    if (t["cm_address"] != null)
                                    {
                                        if (Getdic_address_CMs.ContainsKey((string)t["cm_address"]))
                                        {
                                            cmd.Parameters["@id_cm"].Value = Getdic_address_CMs[(string)t["cm_address"]].id;
                                        }
                                    }
                                    cmd.Parameters["@name"].Value = (string)t["name"];
                                    cmd.Parameters["@brand"].Value = (string)t["brand"];
                                    rows += cmd.ExecuteNonQuery();
                                }
                            }
                            trans.Commit();                            
                        }
                        catch (Exception f)
                        {
                            trans.Rollback();
                            logger.Log(f.Message + " " + f.StackTrace, EventLogEntryType.Error);
                            return GetResult("ERROR", f.Message, oJson["list"].Count<JToken>(), 0);
                        }
                    }
                }

                lock (locker_customer)
                {
                    dic_name_customer = null;
                }

                return GetResult("SUCCESS", "Created " + rows + " rows.", oJson["list"].Count<JToken>(), rows);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                return GetResult("ERROR", e.Message, 0, 0);
            }
        }

        // POST api/LogEntry
        [HttpPost("LogEntry")]
        public Result insertLogentry([FromBody] List<LogEntry> value)
        {
            try
            {
                string agent_uuid = Request.Headers["uuid"];
                if (!Authorized(agent_uuid))
                {
                    logger.Log("UUID not authorized: " + Request.Headers["uuid"], EventLogEntryType.Warning);
                    return GetResult("ERROR", "Not Authorized", 0, 0);
                }

                if (Getdic_uuid_agents.ContainsKey(agent_uuid))
                {
                    SaveAgentCall(value, AppDomain.CurrentDomain.BaseDirectory + "AgentRequests\\" + agent_uuid + "_" + 
                        Getdic_uuid_agents[agent_uuid].label + ".json");
                }                

                int rows_logentry = 0;
                int rows_hardware = 0;
                long id_logentry;
                string command_logentry;
                string command_hardware;
                object res;

                string insert_logentry = @"INSERT INTO logentry (uuid, hostname, description, status, last_heartbeat, last_booted, current_datetime, 
                      timezone, inventory_status, clock_offset, screen_resolution, disk_space, json_text, timestamp, player_name, 
                      channel, channel_res, frameset_res, id_customer, id_cm, id_agent, customer, remaining_items, remaining_mb, player_timestamp, last_update, groups) 
                      VALUES (@uuid, @hostname, @description, @status, @last_heartbeat,
                      @last_booted, @current_datetime, @timezone, @inventory_status, @clock_offset, @screen_resolution, @disk_space, @json_text, 
                      @timestamp, @player_name, @channel, @channel_res, @frameset_res, @id_customer, 
                      @id_cm, @id_agent, @customer, @remaining_items, @remaining_mb, @player_timestamp, @last_update, @groups)";
                string update_logentry = @"UPDATE logentry 
                      SET hostname = @hostname, status = @status, last_heartbeat = @last_heartbeat, last_booted = @last_booted, 
                      current_datetime = @current_datetime, timezone = @timezone, inventory_status = @inventory_status, description = @description, 
                      clock_offset = @clock_offset, screen_resolution = @screen_resolution, disk_space = @disk_space, json_text = @json_text, 
                      timestamp = @timestamp, id_customer = @id_customer, player_name = @player_name, channel = @channel, channel_res = @channel_res, 
                      frameset_res = @frameset_res, customer = @customer, remaining_items = @remaining_items, remaining_mb = @remaining_mb, 
                      player_timestamp = @player_timestamp, last_update = @last_update, groups = @groups 
                      WHERE uuid = @uuid AND id_cm = @id_cm AND id_agent = @id_agent";
                string insert_hardware = @"INSERT INTO hardware (hw_type, status, serialnumber, last_heartbeat, json_text, timestamp, 
                      id_entry) VALUES (@hw_type, @status, @serialnumber, @last_heartbeat, @json_text, @timestamp, @id_entry)";
                string update_hardware = @"UPDATE hardware 
                      SET status = @status, last_heartbeat = @last_heartbeat, json_text = @json_text, timestamp = @timestamp 
                      WHERE serialnumber = @serialnumber AND id_entry = @id_entry";
                string query_logentry = "SELECT id FROM logentry WHERE uuid = @uuid AND id_cm = @id_cm AND id_agent = @id_agent";
                string query_hardware = "SELECT id FROM hardware WHERE serialnumber = @serialnumber AND id_entry = @id_entry";

                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            foreach (LogEntry l in value)
                            {
                                //if (!Getdic_name_clients.ContainsKey(l.client))
                                //{
                                //    throw new Exception("Missing client: " + l.client);
                                //}
                                using (var cmd_check_logentry = new NpgsqlCommand(query_logentry, conn, trans))
                                {
                                    cmd_check_logentry.Parameters.Add(new NpgsqlParameter("@uuid", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd_check_logentry.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint));
                                    cmd_check_logentry.Parameters.Add(new NpgsqlParameter("@id_agent", NpgsqlTypes.NpgsqlDbType.Bigint));

                                    cmd_check_logentry.Parameters["@uuid"].Value = DBvalue(l.uuid);
                                    cmd_check_logentry.Parameters["@id_cm"].Value = DBvalue(Getdic_uuid_agents[agent_uuid].id_cm);
                                    cmd_check_logentry.Parameters["@id_agent"].Value = DBvalue(Getdic_uuid_agents[agent_uuid].id);

                                    res = cmd_check_logentry.ExecuteScalar();
                                }

                                if (res == null)
                                {
                                    command_logentry = insert_logentry;
                                }
                                else
                                {
                                    command_logentry = update_logentry;
                                }
                                using (var cmd = new NpgsqlCommand(command_logentry, conn, trans))
                                {
                                    cmd.Parameters.Add(new NpgsqlParameter("@uuid", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint));
                                    cmd.Parameters.Add(new NpgsqlParameter("@id_agent", NpgsqlTypes.NpgsqlDbType.Bigint));
                                    cmd.Parameters.Add(new NpgsqlParameter("@id_customer", NpgsqlTypes.NpgsqlDbType.Bigint));
                                    cmd.Parameters.Add(new NpgsqlParameter("@hostname", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@status", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@description", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@last_heartbeat", NpgsqlTypes.NpgsqlDbType.Timestamp));
                                    cmd.Parameters.Add(new NpgsqlParameter("@last_booted", NpgsqlTypes.NpgsqlDbType.Timestamp));
                                    cmd.Parameters.Add(new NpgsqlParameter("@player_name", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@channel", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@channel_res", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@frameset_res", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@current_datetime", NpgsqlTypes.NpgsqlDbType.Timestamp));
                                    cmd.Parameters.Add(new NpgsqlParameter("@timezone", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@inventory_status", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@clock_offset", NpgsqlTypes.NpgsqlDbType.Bigint));
                                    cmd.Parameters.Add(new NpgsqlParameter("@screen_resolution", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@disk_space", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@json_text", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@timestamp", NpgsqlTypes.NpgsqlDbType.Timestamp));
                                    cmd.Parameters.Add(new NpgsqlParameter("@customer", NpgsqlTypes.NpgsqlDbType.Text));
                                    cmd.Parameters.Add(new NpgsqlParameter("@remaining_items", NpgsqlTypes.NpgsqlDbType.Integer));
                                    cmd.Parameters.Add(new NpgsqlParameter("@remaining_mb", NpgsqlTypes.NpgsqlDbType.Double));
                                    cmd.Parameters.Add(new NpgsqlParameter("@player_timestamp", NpgsqlTypes.NpgsqlDbType.Timestamp));
                                    cmd.Parameters.Add(new NpgsqlParameter("@last_update", NpgsqlTypes.NpgsqlDbType.Timestamp));
                                    cmd.Parameters.Add(new NpgsqlParameter("@groups", NpgsqlTypes.NpgsqlDbType.Text));

                                    cmd.Parameters["@uuid"].Value = DBvalue(l.uuid);
                                    cmd.Parameters["@id_cm"].Value = DBvalue(Getdic_uuid_agents[agent_uuid].id_cm);
                                    cmd.Parameters["@id_agent"].Value = DBvalue(Getdic_uuid_agents[agent_uuid].id);
                                    cmd.Parameters["@id_customer"].Value = DBvalue(GetCustomerId(l.customer));
                                    cmd.Parameters["@hostname"].Value = DBvalue(l.hostname);
                                    cmd.Parameters["@status"].Value = DBvalue(l.status);
                                    cmd.Parameters["@description"].Value = DBvalue(l.description);
                                    cmd.Parameters["@last_heartbeat"].Value = DBvalue(l.lastHeartbeat);
                                    cmd.Parameters["@last_booted"].Value = DBvalue(l.lastBooted);
                                    cmd.Parameters["@player_name"].Value = DBvalue(l.player_name);
                                    cmd.Parameters["@channel"].Value = DBvalue(StringListToString(l.channel));
                                    cmd.Parameters["@channel_res"].Value = DBvalue(StringListToString(l.channelRes));
                                    cmd.Parameters["@frameset_res"].Value = DBvalue(StringListToString(l.framesetRes));
                                    cmd.Parameters["@current_datetime"].Value = DBvalue(l.currentDatetime);
                                    cmd.Parameters["@timezone"].Value = DBvalue(l.timezone);
                                    cmd.Parameters["@inventory_status"].Value = DBvalue(l.inventoryStatus);
                                    cmd.Parameters["@clock_offset"].Value = DBvalue(l.clockOffset);
                                    cmd.Parameters["@screen_resolution"].Value = DBvalue(l.screenResolution);
                                    cmd.Parameters["@disk_space"].Value = DBvalue(l.diskSpace);
                                    cmd.Parameters["@json_text"].Value = DBvalue(JObject.FromObject(l).ToString());
                                    cmd.Parameters["@timestamp"].Value = DBvalue(DateTime.Now);
                                    cmd.Parameters["@customer"].Value = DBvalue(l.customer);
                                    cmd.Parameters["@remaining_items"].Value = DBvalue(l.remainingItems);
                                    cmd.Parameters["@remaining_mb"].Value = DBvalue(l.remainingMB);
                                    cmd.Parameters["@player_timestamp"].Value = DBvalue(l.playerTimestamp);
                                    cmd.Parameters["@last_update"].Value = DBvalue(l.lastUpdate);
                                    cmd.Parameters["@groups"].Value = DBvalue(StringListToString(l.groups));

                                    rows_logentry += cmd.ExecuteNonQuery();

                                    if (res == null)
                                    {
                                        using (var cmd2 = new NpgsqlCommand(query_logentry, conn, trans))
                                        {
                                            cmd2.Parameters.Add(new NpgsqlParameter("@uuid", NpgsqlTypes.NpgsqlDbType.Text));
                                            cmd2.Parameters.Add(new NpgsqlParameter("@id_cm", NpgsqlTypes.NpgsqlDbType.Bigint));
                                            cmd2.Parameters.Add(new NpgsqlParameter("@id_agent", NpgsqlTypes.NpgsqlDbType.Bigint));

                                            cmd2.Parameters["@uuid"].Value = DBvalue(l.uuid);
                                            cmd2.Parameters["@id_cm"].Value = DBvalue(Getdic_uuid_agents[agent_uuid].id_cm);
                                            cmd2.Parameters["@id_agent"].Value = DBvalue(Getdic_uuid_agents[agent_uuid].id);

                                            id_logentry = (long)cmd2.ExecuteScalar();
                                        }
                                    }
                                    else
                                    {
                                        id_logentry = (long)res;
                                    }

                                    foreach (Hardware h in l.hardware) 
                                    {
                                        using (var cmd_check_hardware = new NpgsqlCommand(query_hardware, conn, trans))
                                        {
                                            cmd_check_hardware.Parameters.Add(new NpgsqlParameter("@serialnumber", NpgsqlTypes.NpgsqlDbType.Text));
                                            cmd_check_hardware.Parameters.Add(new NpgsqlParameter("@id_entry", NpgsqlTypes.NpgsqlDbType.Bigint));

                                            cmd_check_hardware.Parameters["@serialnumber"].Value = DBvalue(h.serialNumber);
                                            cmd_check_hardware.Parameters["@id_entry"].Value = DBvalue(id_logentry);

                                            res = cmd_check_hardware.ExecuteScalar();
                                        }

                                        if (res == null)
                                        {
                                            command_hardware = insert_hardware;
                                        }
                                        else
                                        {
                                            command_hardware = update_hardware;
                                        }

                                        using (var cmd3 = new NpgsqlCommand(command_hardware, conn, trans)) 
                                        {
                                            cmd3.Parameters.Add(new NpgsqlParameter("@hw_type", NpgsqlTypes.NpgsqlDbType.Text));
                                            cmd3.Parameters.Add(new NpgsqlParameter("@status", NpgsqlTypes.NpgsqlDbType.Text));
                                            cmd3.Parameters.Add(new NpgsqlParameter("@serialnumber", NpgsqlTypes.NpgsqlDbType.Text));
                                            cmd3.Parameters.Add(new NpgsqlParameter("@last_heartbeat", NpgsqlTypes.NpgsqlDbType.Timestamp));
                                            cmd3.Parameters.Add(new NpgsqlParameter("@timestamp", NpgsqlTypes.NpgsqlDbType.Timestamp));
                                            cmd3.Parameters.Add(new NpgsqlParameter("@id_entry", NpgsqlTypes.NpgsqlDbType.Bigint));
                                            cmd3.Parameters.Add(new NpgsqlParameter("@json_text", NpgsqlTypes.NpgsqlDbType.Text));

                                            cmd3.Parameters["@timestamp"].Value = DBvalue(DateTime.Now);
                                            cmd3.Parameters["@status"].Value = DBvalue(h.status);
                                            cmd3.Parameters["@serialnumber"].Value = DBvalue(h.serialNumber);
                                            cmd3.Parameters["@last_heartbeat"].Value = DBvalue(h.lastHeartbeat);
                                            cmd3.Parameters["@hw_type"].Value = DBvalue(h.hwType);
                                            cmd3.Parameters["@id_entry"].Value = DBvalue(id_logentry);
                                            cmd3.Parameters["@json_text"].Value = DBvalue(JObject.FromObject(h).ToString());

                                            rows_hardware += cmd3.ExecuteNonQuery();
                                        }
                                    }
                                }
                            }                            
                            trans.Commit();
                        }
                        catch (Exception f)
                        {
                            trans.Rollback();
                            logger.Log(f.Message + " " + f.StackTrace, EventLogEntryType.Error);
                            return GetResult("ERROR", f.Message, value.Count(), 0);
                        }
                    }
                }

                string resDesc = "Managed " + rows_logentry + " logentry rows and " + rows_hardware + " hardware rows.";
                return GetResult("SUCCESS", resDesc, value.Count(), rows_logentry);
            }
            catch (Exception e)
            {
                logger.Log(e.Message + " " + e.StackTrace, EventLogEntryType.Error);
                return GetResult("ERROR", e.Message, value.Count(), 0);
            }
        }

        #endregion

        #region PUT
        //// PUT api/<ScairimInterfaceController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}
        #endregion

        #region DELETE
        //// DELETE api/<ScairimInterfaceController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
        #endregion

        #region init Dictionaries

        private static void initAgents()
        {
            using var conn = new NpgsqlConnection(connString);
            conn.Open();
            var sql = "SELECT * FROM agents";
            using var cmd = new NpgsqlCommand(sql, conn);

            using var reader = cmd.ExecuteReader();

            dic_uuid_agents = new Dictionary<string, Agent>();

            while (reader.Read())
            {
                Agent a = new Agent
                {
                    id = reader.GetInt32(reader.GetOrdinal("id")),
                    id_cm = reader.GetInt32(reader.GetOrdinal("id_cm")),
                    uuid = reader.GetString(reader.GetOrdinal("uuid"))
                };
                if (!reader.IsDBNull(reader.GetOrdinal("label"))) a.label = reader.GetString(reader.GetOrdinal("label"));
  
                dic_uuid_agents.Add(a.uuid, a);
            }
        }

        private static void initCMs()
        {
            using var conn = new NpgsqlConnection(connString);
            conn.Open();
            var sql = "SELECT * FROM cms";
            using var cmd = new NpgsqlCommand(sql, conn);

            using var reader = cmd.ExecuteReader();

            dic_address_CMs = new Dictionary<string, ContentManager>();

            while (reader.Read())
            {
                ContentManager cm = new ContentManager
                {
                    id = reader.GetInt32(reader.GetOrdinal("id")),
                    address = reader.GetString(reader.GetOrdinal("address")),
                    label = reader.GetString(reader.GetOrdinal("label")),
                    platform = reader.GetString(reader.GetOrdinal("platform")),
                    timezone = reader.GetString(reader.GetOrdinal("timezone"))
                };
                dic_address_CMs.Add(cm.address, cm);
            }
        }

        private static void initCustomers()
        {
            using var conn = new NpgsqlConnection(connString);
            conn.Open();
            var sql = "SELECT * FROM customers";
            using var cmd = new NpgsqlCommand(sql, conn);

            using var reader = cmd.ExecuteReader();

            dic_name_customer = new Dictionary<string, Customer>();

            while (reader.Read())
            {
                Customer c = new Customer
                {
                    id = reader.GetInt32(reader.GetOrdinal("id")),
                    name = reader.GetString(reader.GetOrdinal("name")),
                    brand = reader.GetString(reader.GetOrdinal("brand")),
                    id_cm = reader.GetInt32(reader.GetOrdinal("id_cm"))
                };
                dic_name_customer.Add(c.name, c);
            }
        }

        #endregion

        private bool Authorized(string uuid)
        {
            if (uuid == null)
            {
                return false;
            }
            else if (Getdic_uuid_agents.ContainsKey(uuid))
            {
                return true;
            }
            else 
            {
                return false;
            }
        }

        private bool AdminAuthorized(string secret)
        {
            if (secret == null)
            {
                return false;
            }
            else if (aAdminSecrets.Contains(secret))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private object DBvalue(object value) 
        {
            if (value == null)
            {
                return DBNull.Value;
            }
            else
            {
                return value;
            }
        }

        private object GetCustomerId(string customer)
        {
            if (customer != null && Getdic_name_customer.ContainsKey(customer))
            {
                return Getdic_name_customer[customer].id;
            }
            else 
            {
                return null;
            }
        }

        private string StringListToString(List<string> ls)
        {
            if (ls == null)
            {
                return null;
            }

            string tmp = "";
            
            foreach (string s in ls)
            {
                if (tmp == "")
                {
                    tmp = s;
                }
                else
                {
                    tmp = tmp + ";" + s;
                }
            }
            return tmp;
        }

        private List<string> StringToStringList(string s)
        {
            if (s == null)
            {
                return null;
            }
            List<string> tmp = new List<string>();
            foreach (string str in s.Split(";"))
            {
                tmp.Add(str);
            }
            return tmp;
        }

        private Result GetResult(string result, string description, int requests, int managed)
        {
            Result res = new Result();
            res.result = result;
            res.data = new ReturnedData { description = description, requests = requests, processed = managed };
            return res;
        }

        private void SaveAgentCall(List<LogEntry> lsEntries, string filename)
        {
            string json = System.Text.Json.JsonSerializer.Serialize(lsEntries);
            System.IO.File.WriteAllText(filename, json);
        }
    }
}
